package ru.kazakov.jse44.repository;

import ru.kazakov.jse44.entity.Customer;

import java.util.Optional;

public interface CustomerRepository extends Repository<Customer>{

    Optional<Customer> findById(Integer id);

    void removeById(Integer id);

}
