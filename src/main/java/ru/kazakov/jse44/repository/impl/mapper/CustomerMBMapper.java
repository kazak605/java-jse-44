package ru.kazakov.jse44.repository.impl.mapper;

import org.apache.ibatis.annotations.*;
import ru.kazakov.jse44.entity.Account;
import ru.kazakov.jse44.entity.Customer;
import ru.kazakov.jse44.repository.impl.mapper.provider.Provider;

import java.util.Date;
import java.util.List;

public interface CustomerMBMapper {

    @Insert("insert into customer(first_name, last_name, birth_date) values (#{firstName}, #{lastName}, (to_date(cast(#{birthDate} as text),'YYYY-MM-DD')")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer createCustomer(Customer customer);

    @Update("update customer set first_name = #{firstName}, last_name = #{lastNmae}, birth_date = to_date(cast(#{birthDate} as text),'YYYY-MM-DD') where id = #{id}")
    void updateCustomer(Customer customer);

    @Delete("delete from customer where id = #{id}")
    void deleteById(Integer id);

    @Select("select * from customer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date")
    })
    List<Customer> getAllCustomers();

    @Select("select * from customer where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date")
    })
    Customer getById(Integer id);

    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "birthDate", column = "birth_date"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name")
    })
    List<Customer> findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);

}
