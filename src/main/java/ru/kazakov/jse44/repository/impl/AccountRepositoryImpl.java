package ru.kazakov.jse44.repository.impl;

import org.apache.ibatis.session.SqlSession;
import ru.kazakov.jse44.entity.Account;
import ru.kazakov.jse44.repository.AccountRepository;
import ru.kazakov.jse44.repository.datasource.MyBatisUtil;
import ru.kazakov.jse44.repository.impl.mapper.AccountMBMapper;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {

    private static volatile AccountRepositoryImpl instance = null;

    private AccountRepositoryImpl() {
    }

    public static AccountRepositoryImpl getInstance() {
        if (instance == null) {
            synchronized (AccountRepositoryImpl.class) {
                if (instance == null) {
                    instance = new AccountRepositoryImpl();
                }
            }
        }
        return instance;

    }

    @Override
    public Optional<Account> create(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.createAccount(input);
            sqlSession.commit();
            return Optional.of(input);
        }

    }

    @Override
    public Optional<Account> update(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.updateAccount(input);
            sqlSession.commit();
            return Optional.of(input);
        }

    }

    @Override
    public void removeByAccountNumber(String AccountNumber) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.deleteByAccountNumber(AccountNumber);
            sqlSession.commit();
        }
    }

    @Override
    public List<Account> findAll() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return accountMBMapper.getAllAccounts();
        }
    }

    @Override
    public Optional<Account> findByAccountNumber(String AccountNumber) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return Optional.ofNullable(accountMBMapper.getByAccountNumber(AccountNumber));
        }
    }

    @Override
    public List<Account> findItems(Integer id, String firstName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return accountMBMapper.findItems(id, firstName, lastName, bDay, account, balance);
        }
    }

}
