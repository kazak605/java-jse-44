package ru.kazakov.jse44.repository.impl.mapper.provider;

import org.apache.ibatis.jdbc.SQL;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Provider {

    public String findCustomers(Integer id, String firstName, String lastName, Date bDay, String account, Double balance) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return new SQL() {{
            SELECT("customer.id, customer.first_name, customer.last_name, customer.birth_date, account.account_number, account.ballance");
            FROM("customer");
            LEFT_OUTER_JOIN("account on customer.id = account.customer_id");
            boolean hasCondition = false;
            if (id != null && id > 0) {
                WHERE("account.customer_id = '" + id + "'");
                hasCondition = true;
            }
            if (firstName != null && !firstName.isEmpty()) {
                if (hasCondition) {
                    AND();
                }
                WHERE("customer.first_name like '%" + firstName + "%'");
                hasCondition = true;
            }
            if (lastName != null && !lastName.isEmpty()) {
                if (hasCondition) {
                    AND();
                }
                WHERE("customer.last_name like '%" + lastName + "%'");
                hasCondition = true;
            }
            if (bDay != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("customer.birth_date ='" + formatter.format(bDay) + "'");
                hasCondition = true;
            }
            if (account != null && !account.isEmpty()) {
                if (hasCondition) {
                    AND();
                }
                WHERE("account.account_number ='" + account + "'");
            }
            if (balance != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("account.ballance ='" + balance + "'");
            }
        }}.toString();
    }

}
