package ru.kazakov.jse44.repository.impl;

import org.apache.ibatis.session.SqlSession;
import ru.kazakov.jse44.entity.Customer;
import ru.kazakov.jse44.repository.CustomerRepository;
import ru.kazakov.jse44.repository.datasource.MyBatisUtil;
import ru.kazakov.jse44.repository.impl.mapper.AccountMBMapper;
import ru.kazakov.jse44.repository.impl.mapper.CustomerMBMapper;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CustomerRepositoryImpl implements CustomerRepository {

    private static volatile CustomerRepositoryImpl instance = null;

    private CustomerRepositoryImpl() {
    }

    public static CustomerRepositoryImpl getInstance() {
        if (instance == null) {
            synchronized (AccountRepositoryImpl.class) {
                if (instance == null) {
                    instance = new CustomerRepositoryImpl();
                }
            }
        }
        return instance;

    }

    @Override
    public Optional<Customer> create(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.createCustomer(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Customer> update(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.updateCustomer(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public void removeById(Integer id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.deleteById(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Customer> findAll() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.getAllCustomers();
        }
    }

    @Override
    public Optional<Customer> findById(Integer id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return Optional.ofNullable(customerMBMapper.getById(id));
        }
    }

    @Override
    public List<Customer> findItems(Integer id, String firstName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.findItems(id, firstName, lastName, bDay, account, balance);
        }
    }

}
