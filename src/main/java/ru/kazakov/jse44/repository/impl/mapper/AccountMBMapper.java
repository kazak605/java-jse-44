package ru.kazakov.jse44.repository.impl.mapper;

import org.apache.ibatis.annotations.*;
import ru.kazakov.jse44.entity.Account;
import ru.kazakov.jse44.repository.impl.mapper.provider.Provider;

import java.util.Date;
import java.util.List;

public interface AccountMBMapper {
    @Insert("insert into account(customer_id, account_number, ballance) values (#{customerId}, #{accountNumber}, #{balance})")
    Integer createAccount(Account account);

    @Update("update account set customer_id = #{customerId}, account_number = #{accountNumber}, ballance = #{balance} where account_number = #{accountNumber}")
    void updateAccount(Account account);

    @Delete("delete from account where account_number = #{accountNumber}")
    void deleteByAccountNumber(String accountNumber);

    @Select("select * from account")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "ballance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    List<Account> getAllAccounts();

    @Select("select * from account where account_number = #{accountNumber}")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "ballance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    Account getByAccountNumber(String accountNumber);

    @Select("select * from account where customer_id = #{customerId}")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "ballance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    Account getById(Integer customerId);

    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "customerId", column = "id"),
            @Result(property = "ballance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    List<Account> findItems(Integer id, String firstName, String lastName, Date bDay, String account, Double balance);

}
