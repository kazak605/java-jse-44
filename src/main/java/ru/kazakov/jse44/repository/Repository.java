package ru.kazakov.jse44.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Repository<T> {

    Optional<T> create(T input);

    Optional<T> update(T input);

    List<T> findAll();

    List<T> findItems(Integer id, String firstName, String lastName, Date bDay, String account, Double balance);

}
