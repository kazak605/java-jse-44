package ru.kazakov.jse44.repository;

import ru.kazakov.jse44.entity.Account;

import java.util.Optional;

public interface AccountRepository extends Repository<Account> {

    Optional<Account> findByAccountNumber(String AccountNumber);

    void removeByAccountNumber(String AccountNumber);

}
