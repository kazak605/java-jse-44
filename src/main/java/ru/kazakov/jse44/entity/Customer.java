package ru.kazakov.jse44.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    Integer id;

    String firstName;

    String lastName;

    Date birthDate;

}
