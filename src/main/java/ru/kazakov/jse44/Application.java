package ru.kazakov.jse44;

import ru.kazakov.jse44.controller.impl.AccountControllerImpl;
import ru.kazakov.jse44.controller.impl.CustomerControllerImpl;


import jakarta.xml.ws.Endpoint;

public class Application {
    public static void main(String[] args) {
        AccountControllerImpl accountController = new AccountControllerImpl();
        Endpoint.publish("http://localhost:8899/ws/account", accountController);

        CustomerControllerImpl customerController = new CustomerControllerImpl();
        Endpoint.publish("http://localhost:8899/ws/customer", customerController);
    }
}
