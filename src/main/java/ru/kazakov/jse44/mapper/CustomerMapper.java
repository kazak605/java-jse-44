package ru.kazakov.jse44.mapper;

import ru.kazakov.jse44.dto.CustomerDTO;
import ru.kazakov.jse44.entity.Customer;

public class CustomerMapper {

    public static CustomerDTO toDto(Customer customer) {
        return CustomerDTO.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .build();
    }

    public static Customer fromDto(CustomerDTO customerDTO) {
        return Customer.builder()
                .id(customerDTO.getId())
                .firstName(customerDTO.getFirstName())
                .lastName(customerDTO.getLastName())
                .build();
    }

}
