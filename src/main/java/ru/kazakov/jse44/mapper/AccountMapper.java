package ru.kazakov.jse44.mapper;

import ru.kazakov.jse44.dto.AccountDTO;
import ru.kazakov.jse44.entity.Account;

public class AccountMapper {

    public static AccountDTO toDto (Account account) {
        return AccountDTO.builder()
                .customerId(account.getCustomerId())
                .accountNumber(account.getAccountNumber())
                .ballance(account.getBalance())
                .build();
    }
}
