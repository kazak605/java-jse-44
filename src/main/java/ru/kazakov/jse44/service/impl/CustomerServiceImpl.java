package ru.kazakov.jse44.service.impl;

import ru.kazakov.jse44.dto.*;
import ru.kazakov.jse44.entity.Account;
import ru.kazakov.jse44.entity.Customer;
import ru.kazakov.jse44.enumerated.Status;
import ru.kazakov.jse44.mapper.AccountMapper;
import ru.kazakov.jse44.mapper.CustomerMapper;
import ru.kazakov.jse44.repository.impl.CustomerRepositoryImpl;
import ru.kazakov.jse44.service.CustomerService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CustomerServiceImpl implements CustomerService {

    private static CustomerServiceImpl instance = null;

    private final CustomerRepositoryImpl customerRepository;

    private CustomerServiceImpl() {
        this.customerRepository = CustomerRepositoryImpl.getInstance();
    }

    public static CustomerServiceImpl getInstance() {
        if (instance == null) {
            synchronized (AccountServiceImpl.class) {
                if (instance == null) {
                    instance = new CustomerServiceImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public CustomerResponseDTO create(CustomerDTO customerDTO) {
        Customer customer = CustomerMapper.fromDto(customerDTO);
        Optional<Customer> customerOptional = customerRepository.create(customer);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerResponseDTO update(CustomerDTO customerDTO) {
        Customer customer = CustomerMapper.fromDto(customerDTO);
        Optional<Customer> customerOptional = customerRepository.update(customer);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public void removeById(Integer id) {
        customerRepository.removeById(id);
    }

    @Override
    public CustomerListResponseDTO findAll() {
        List<Customer> customers = customerRepository.findAll();
        CustomerDTO[] customerDTOS = customers.stream().map(account -> CustomerMapper.toDto(account)).toArray(CustomerDTO[]::new);
        return CustomerListResponseDTO.builder().status(Status.OK).payload(customerDTOS).build();
    }

    @Override
    public CustomerResponseDTO findById(Integer id) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payload(CustomerMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerListResponseDTO findCustomers(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return CustomerListResponseDTO.
                builder().
                payload(customerRepository.findItems(id, firsName, lastName, bDay, account, balance).toArray(new CustomerDTO[0]))
                .status(Status.OK)
                .build();
    }
}
