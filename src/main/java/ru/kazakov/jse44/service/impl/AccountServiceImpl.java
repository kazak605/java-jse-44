package ru.kazakov.jse44.service.impl;

import ru.kazakov.jse44.dto.AccountDTO;
import ru.kazakov.jse44.dto.AccountListResponseDTO;
import ru.kazakov.jse44.dto.AccountResponseDTO;
import ru.kazakov.jse44.entity.Account;
import ru.kazakov.jse44.enumerated.Status;
import ru.kazakov.jse44.mapper.AccountMapper;
import ru.kazakov.jse44.repository.impl.AccountRepositoryImpl;
import ru.kazakov.jse44.service.AccountService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AccountServiceImpl implements AccountService {

    private final AccountRepositoryImpl accountRepository;

    private static AccountServiceImpl instance = null;

    private AccountServiceImpl() {
        this.accountRepository = AccountRepositoryImpl.getInstance();
    }

    public static AccountServiceImpl getInstance() {
        if (instance == null) {
            synchronized (AccountServiceImpl.class) {
                if (instance == null) {
                    instance = new AccountServiceImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public AccountResponseDTO create(AccountDTO accountDTO) {
        Account account = Account.builder()
                .customerId(accountDTO.getCustomerId())
                .accountNumber(accountDTO.getAccountNumber())
                .balance(accountDTO.getBallance())
                .build();
        Optional<Account> accountOptional = accountRepository.create(account);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(accountOptional.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountResponseDTO update(AccountDTO accountDTO) {
        Account account = Account.builder()
                .customerId(accountDTO.getCustomerId())
                .accountNumber(accountDTO.getAccountNumber())
                .balance(accountDTO.getBallance())
                .build();
        Optional<Account> accountOptional = accountRepository.update(account);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(accountOptional.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public void removeByAccountNumber(String accountNumber) {
        accountRepository.removeByAccountNumber(accountNumber);
    }

    @Override
    public AccountListResponseDTO findAll() {
        List<Account> accounts = accountRepository.findAll();
        AccountDTO[] accountDTOS = accounts.stream().map(account -> AccountMapper.toDto(account)).toArray(AccountDTO[]::new);
        return AccountListResponseDTO.builder().status(Status.OK).payload(accountDTOS).build();
    }

    @Override
    public AccountResponseDTO findByAccountNumber(String accountNumber) {
        Optional<Account> account = accountRepository.findByAccountNumber(accountNumber);
        if (account.isPresent()) {
            return AccountResponseDTO.builder().payload(AccountMapper.toDto(account.get())).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountListResponseDTO findAccounts(Integer id, String firstName, String lastName, Date bDay, String account, Double balance) {
        return AccountListResponseDTO.
                builder().
                payload(accountRepository.findItems(id, firstName, lastName, bDay, account, balance).toArray(new AccountDTO[0]))
                .status(Status.OK)
                .build();
    }

}
