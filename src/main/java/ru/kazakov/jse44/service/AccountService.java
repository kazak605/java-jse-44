package ru.kazakov.jse44.service;


import ru.kazakov.jse44.dto.AccountDTO;
import ru.kazakov.jse44.dto.AccountListResponseDTO;
import ru.kazakov.jse44.dto.AccountResponseDTO;

import java.util.Date;

public interface AccountService {

    AccountResponseDTO create(AccountDTO accountDTO);

    AccountResponseDTO update(AccountDTO accountDTO);

    void removeByAccountNumber(String accountNumber);

    AccountListResponseDTO findAll();

    AccountResponseDTO findByAccountNumber(String accountNumber);

    AccountListResponseDTO findAccounts(Integer id, String firstName, String lastName, Date bDay, String account, Double balance);
}
