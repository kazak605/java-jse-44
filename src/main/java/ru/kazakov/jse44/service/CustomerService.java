package ru.kazakov.jse44.service;

import ru.kazakov.jse44.dto.CustomerDTO;
import ru.kazakov.jse44.dto.CustomerListResponseDTO;
import ru.kazakov.jse44.dto.CustomerResponseDTO;

import java.util.Date;

public interface CustomerService {

    CustomerResponseDTO create(CustomerDTO customerDTO);

    CustomerResponseDTO update(CustomerDTO customerDTO);

    void removeById(Integer id);

    CustomerListResponseDTO findAll();

    CustomerResponseDTO findById(Integer id);

    CustomerListResponseDTO findCustomers(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);

}
