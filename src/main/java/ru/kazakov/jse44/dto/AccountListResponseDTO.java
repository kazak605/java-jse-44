package ru.kazakov.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kazakov.jse44.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountListResponseDTO {

    private Status status;

    private AccountDTO[] payload;
}
