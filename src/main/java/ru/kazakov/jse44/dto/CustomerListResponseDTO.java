package ru.kazakov.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kazakov.jse44.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerListResponseDTO {

    private Status status;

    private CustomerDTO[] payload;

}
