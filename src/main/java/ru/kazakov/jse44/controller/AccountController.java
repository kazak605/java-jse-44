package ru.kazakov.jse44.controller;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;
import ru.kazakov.jse44.dto.AccountDTO;
import ru.kazakov.jse44.dto.AccountListResponseDTO;
import ru.kazakov.jse44.dto.AccountResponseDTO;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AccountController {

    @WebMethod
    AccountResponseDTO createAccount(AccountDTO accountDTO);

    @WebMethod
    AccountResponseDTO updateAccount(AccountDTO accountDTO);

    @WebMethod
    void deleteAccount(@WebParam(name = "accountNumber") String accountNumber);

    @WebMethod
    AccountListResponseDTO findAll();

    @WebMethod
    AccountResponseDTO findByAccountNumber(@WebParam(name = "accountNumber") String accountNumber);


}
