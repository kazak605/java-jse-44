package ru.kazakov.jse44.controller.impl;

import jakarta.jws.WebService;
import ru.kazakov.jse44.controller.AccountController;
import ru.kazakov.jse44.dto.AccountDTO;
import ru.kazakov.jse44.dto.AccountListResponseDTO;
import ru.kazakov.jse44.dto.AccountResponseDTO;
import ru.kazakov.jse44.service.impl.AccountServiceImpl;


@WebService(endpointInterface = "ru.kazakov.jse44.controller.AccountController")
public class AccountControllerImpl implements AccountController {

    private AccountServiceImpl accountService;

    public AccountControllerImpl() {
        this.accountService = AccountServiceImpl.getInstance();
    }

    @Override
    public AccountResponseDTO createAccount(AccountDTO accountDTO) {
        return accountService.create(accountDTO);
    }

    @Override
    public AccountResponseDTO updateAccount(AccountDTO accountDTO) {
        return accountService.update(accountDTO);
    }

    @Override
    public void deleteAccount(String accountNumber) {
        accountService.removeByAccountNumber(accountNumber);
    }

    @Override
    public AccountListResponseDTO findAll() {
        return accountService.findAll();
    }

    @Override
    public AccountResponseDTO findByAccountNumber(String accountNumber) {
        return accountService.findByAccountNumber(accountNumber);
    }

}
