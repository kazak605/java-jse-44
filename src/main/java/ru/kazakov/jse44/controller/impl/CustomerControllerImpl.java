package ru.kazakov.jse44.controller.impl;

import jakarta.jws.WebService;
import ru.kazakov.jse44.controller.CustomerController;
import ru.kazakov.jse44.dto.CustomerDTO;
import ru.kazakov.jse44.dto.CustomerListResponseDTO;
import ru.kazakov.jse44.dto.CustomerResponseDTO;
import ru.kazakov.jse44.service.impl.CustomerServiceImpl;

@WebService(endpointInterface = "ru.kazakov.jse44.controller.CustomerController")
public class CustomerControllerImpl implements CustomerController {

    private CustomerServiceImpl customerService;

    public CustomerControllerImpl() {
        this.customerService = CustomerServiceImpl.getInstance();
    }

    @Override
    public CustomerResponseDTO createCustomer(CustomerDTO customerDTO) {
        return customerService.create(customerDTO);
    }

    @Override
    public CustomerResponseDTO updateCustomer(CustomerDTO customerDTO) {
        return customerService.update(customerDTO);
    }

    @Override
    public void deleteCustomer(Integer id) {
        customerService.removeById(id);
    }

    @Override
    public CustomerListResponseDTO findAll() {
        return customerService.findAll();
    }

    @Override
    public CustomerResponseDTO findById(Integer id) {
        return customerService.findById(id);
    }

}
