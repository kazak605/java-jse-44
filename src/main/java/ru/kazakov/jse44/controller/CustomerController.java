package ru.kazakov.jse44.controller;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;
import ru.kazakov.jse44.dto.AccountResponseDTO;
import ru.kazakov.jse44.dto.CustomerDTO;
import ru.kazakov.jse44.dto.CustomerListResponseDTO;
import ru.kazakov.jse44.dto.CustomerResponseDTO;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface CustomerController {

    @WebMethod
    CustomerResponseDTO createCustomer(CustomerDTO customerDTO);

    @WebMethod
    CustomerResponseDTO updateCustomer(CustomerDTO customerDTO);

    @WebMethod
    void deleteCustomer(@WebParam(name = "id") Integer id);

    @WebMethod
    CustomerListResponseDTO findAll();


    @WebMethod
    CustomerResponseDTO findById(@WebParam(name = "id") Integer id);

}
